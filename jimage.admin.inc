<?php

/**
 * @file
 * Admin page callbacks for the jimage module.
 *
 */

/**
 * Configuration callback for this module.
 */

function jimage_admin_settings() {
  $form = array();
  // Image Style Settings.
  $form['jimage_posting'] = array(
    '#type'    => 'fieldset',
    '#collapsible'    => TRUE,
    '#collapsed' => TRUE,
    '#title'          => t('Display Jimage Style'),
    '#weight'         => -70
   );
  $options = drupal_map_assoc(array(t('none'), t('viewimage'), t('slideimage'), t('lightbox'), t('lightshow')));
  $form['jimage_posting']['jimage_display'] = array(
    '#type' => 'radios',
    '#title' => t('Display Jimage Style'),
    '#default_value' => variable_get('jimage_display', 'slideimage'),
    '#options' => $options,
  );
  // Small Settings.
  $form['jimage_small'] = array(
    '#type'    => 'fieldset',
    '#collapsible'    => TRUE,
    '#collapsed' => TRUE,
    '#title'          => t('Small Image settings'),
    '#weight'         => -50
   );
  $form['jimage_small']['idev_small_create'] = array(
    '#type' => 'radios',
    '#title' => t('Create Small images'),
    '#description' => t('If enabled, Small images will be created.'),
    '#default_value' => variable_get('idev_small_create', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['jimage_small']['idev_small_show'] = array(
    '#type' => 'radios',
    '#title' => t('Show Small images in teaser'),
    '#description' => t('If enabled, Small images will be created.'),
    '#default_value' => variable_get('idev_small_show', 1),
    '#options' => array(t('No'), t('Yes')),
  );
  $form['jimage_small']['idev_small_width'] = array(
    '#type'   => 'textfield',
    '#title'   => t('Width'),
    '#default_value' => variable_get('idev_small_width', 80),
    '#required'  => FALSE,
    '#size'   => 30,
    '#maxlength'  => 30,
  );
  $form['jimage_small']['idev_small_height'] = array(
    '#type'   => 'textfield',
    '#title'   => t('Height'),
    '#default_value' => variable_get('idev_small_height', 80),
    '#required'  => FALSE,
    '#size'   => 30,
    '#maxlength'  => 30,
  );
  $form['jimage_small']['idev_small_actions'] = array(
    '#type' => 'select',
    '#title' => t('Select Action'),
    '#default_value' => variable_get('idev_small_actions', 'scale_and_crop'),
    '#required'  => FALSE,
    '#options' => array(
      'scale_and_crop' => t('Scale and crop'),
      'scale' => t('Scale'),
      'resize' => t('Resize'),
    ),
    '#description' => t('Please choose an (image file manipulations) option.'),
  );
  // Thumbnail Settings.
  $form['jimage_thumbnail'] = array(
    '#type'    => 'fieldset',
    '#collapsible'    => TRUE,
    '#collapsed' => TRUE,
    '#title'          => t('Thumbnail Image settings'),
    '#weight'         => -40
   );
  $form['jimage_thumbnail']['idev_thumb_create'] = array(
    '#type' => 'radios',
    '#title' => t('Create Thumbnail images'),
    '#description' => t('If enabled, Thumbnail imagess will be created.'),
    '#default_value' => variable_get('idev_thumb_create', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['jimage_thumbnail']['idev_thumb_width'] = array(
    '#type'   => 'textfield',
    '#title'   => t('Width'),
    '#default_value' => variable_get('idev_thumb_width', 100),
    '#required'  => FALSE,
    '#size'   => 30,
    '#maxlength'  => 30,
  );
  $form['jimage_thumbnail']['idev_thumb_height'] = array(
    '#type'   => 'textfield',
    '#title'   => t('Height'),
    '#default_value' => variable_get('idev_thumb_height', 100),
    '#required'  => FALSE,
    '#size'   => 30,
    '#maxlength'  => 30,
  );
  $form['jimage_thumbnail']['idev_thumb_actions'] = array(
    '#type' => 'select',
    '#title' => t('Select Action'),
    '#default_value' => variable_get('idev_thumb_actions', 'scale_and_crop'),
    '#required'  => FALSE,
    '#options' => array(
      'scale_and_crop' => t('Scale and crop'),
      'scale' => t('Scale'),
      'resize' => t('Resize'),
    ),
    '#description' => t('Please choose an (image file manipulations) option.'),
  );
  // Main Settings
  $form['jimage_main'] = array(
    '#type'    => 'fieldset',
    '#collapsible'    => TRUE,
    '#collapsed' => TRUE,
    '#title'          => t('Main Image settings'),
    '#weight'         => -30
   );
  $form['jimage_main']['idev_main_create'] = array(
    '#type' => 'radios',
    '#title' => t('Create Main images'),
    '#description' => t('If enabled, Main images will be created.'),
    '#default_value' => variable_get('idev_main_create', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
  );
  $form['jimage_main']['idev_main_width'] = array(
    '#type'   => 'textfield',
    '#title'   => t('Width'),
    '#default_value' => variable_get('idev_main_width', 475),
    '#required'  => FALSE,
    '#size'   => 30,
    '#maxlength'  => 30,
  );
  $form['jimage_main']['idev_main_height'] = array(
    '#type'   => 'textfield',
    '#title'   => t('Height'),
    '#default_value' => variable_get('idev_main_height', 500),
    '#required'  => FALSE,
    '#size'   => 30,
    '#maxlength'  => 30,
  );
  $form['jimage_main']['idev_main_actions'] = array(
    '#type' => 'select',
    '#title' => t('Select Action'),
    '#default_value' => variable_get('idev_main_actions', 'scale_and_crop'),
    '#required'  => FALSE,
    '#options' => array(
      'scale_and_crop' => t('Scale and crop'),
      'scale' => t('Scale'),
      'resize' => t('Resize'),
    ),
    '#description' => t('Please choose an (image file manipulations) option.'),
  );

  //$form['#validate'] = array('jimage_admin_settings_validate');

  return system_settings_form($form);
}

function jimage_admin_settings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['idev_small_width']) || $form_state['values']['idev_small_width'] < 0 || $form_state['values']['idev_small_width'] > IDEV_MAX_WIDTH) {
    form_set_error('idev_small_width', t("%value is not a valid width-jimage setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['idev_small_width'])));
  }
  if (!is_numeric($form_state['values']['idev_small_height']) || $form_state['values']['idev_small_height'] < 0 || $form_state['values']['idev_small_height'] > IDEV_MAX_HEIGHT) {
    form_set_error('idev_small_height', t("%value is not a valid height-jimage setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['idev_small_height'])));
  }
  if (!is_numeric($form_state['values']['idev_thumb_width']) || $form_state['values']['idev_thumb_width'] < 0 || $form_state['values']['idev_thumb_width'] > IDEV_MAX_WIDTH) {
    form_set_error('idev_thumb_width', t("%value is not a valid width-jimage setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['idev_thumb_width'])));
  }
  if (!is_numeric($form_state['values']['idev_thumb_height']) || $form_state['values']['idev_thumb_height'] < 0 || $form_state['values']['idev_thumb_height'] > IDEV_MAX_HEIGHT) {
    form_set_error('idev_thumb_height', t("%value is not a valid height-jimage setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['idev_thumb_height'])));
  }
  if (!is_numeric($form_state['values']['idev_main_width']) || $form_state['values']['idev_main_width'] < 0 || $form_state['values']['idev_main_width'] > IDEV_MAX_WIDTH) {
    form_set_error('idev_main_width', t("%value is not a valid width-jimage setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['idev_main_width'])));
  }
  if (!is_numeric($form_state['values']['idev_main_height']) || $form_state['values']['idev_main_height'] < 0 || $form_state['values']['idev_main_height'] > IDEV_MAX_HEIGHT) {
    form_set_error('idev_main_height', t("%value is not a valid height-jimage setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['idev_main_height'])));
  }
}