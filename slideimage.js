
$(document).ready(function() {
  $(".imageOptions a").click(function() {
    var imageSource = $(this).attr("href");
    var imageTitle = $(this).attr("title");
    //$(".loader").addClass("loading");
    //$("h3").remove();
    showImage(imageSource,imageTitle);
    return false;
  });
});

function showImage(src, alt) {
  $(".loader img").fadeOut("normal").remove();
  var largeImage = new Image();
  $(largeImage).load(function() {
    $(this).hide();
    $(".loader").append(this).removeClass("loading");
    $(this).fadeIn("slow");              
  });    
  //$(largeImage).attr("src", src);     
  $(largeImage).attr({ src: src, alt: alt });
}