<?php
/* $Id */

/**
 * jimage theme settings.
 *
 * @ingroup themeable
 */

/**
 * Displays file attachments in table
 *
 * @ingroup themeable
 */
function theme_jimage_teaser($jimage) {
  // Display teaser image
  $imgpath = $jimage['path'];
  $output  = '';
  $text    = '<img src="' . $imgpath->smallpath . '" />';
  $path    = 'node/' . $imgpath->nid;
  $options = array(
    'html' => TRUE,
  );
  $output .= '<div class="jimageteaser">';
  $output .= l($text, $path, $options);
  $output .= '</div>';
  return $output;
} 
 
/**
 * Displays file attachments in slideimage style
 *
 * @ingroup themeable
 */
function theme_jimage_slideimage($jimage) {
  // Display main image
  $imgfiles = $jimage['image'];
  $imgpath  = $jimage['path'];
  drupal_add_js(drupal_get_path('module', 'jimage') .'/slideimage.js');
  //drupal_add_css(drupal_get_path('module', 'jimage') .'/jimage.css');
  $output  = '';
  $output .= '<div class="container">';
  $output .= '<div class="loader">';
  $output .= '<h5>' . IDEV_MAIN_TEXT . '</h5>';
  $text    = '<img src="' . base_path() . $imgpath->mainpath . '" />';
  $options = array(
    'html' => TRUE,
  );
  $output .= l($text, 'node/' . $imgpath->nid, $options);
  $output .= '</div>';
  $output .= '<br class="clearboth">'; 
  $output .= '<p class="imageOptions">';
  // Display thumbnails
  foreach ($imgfiles as $imgfile) {
    $imgfile = (object)$imgfile;
    if ($imgfile->list && empty($imgfile->remove)) {
      $title    = $imgfile->description ? $imgfile->description : $imgfile->filename;
      $text    = '<img src="' . base_path() . $imgfile->thumbpath . '" />';
      $options = array(
        'attributes' => array(
          'title' => t('@title', array('@title' => $title)),
        ),
        'html' => TRUE,        
      );
      $output .= l($text, $imgfile->mainpath, $options);
    }
  }
  $output .= '</p>';
  $output .= '</div>';
  return $output;
}

/**
 * Displays file attachments images
 *
 * @ingroup themeable
 */
function theme_jimage_viewimage($jimage) {
  // Display main image
  $imgfiles = $jimage['image'];
  $imgpath  = $jimage['path'];
  drupal_add_css(drupal_get_path('module', 'jimage') .'/jimage.css');
  $output  = '';
  $output .= '<div class="container">';
  $output .= '<br class="clearboth">'; 
  $output .= '<p class="imageview">';
  // Display images
  foreach ($imgfiles as $imgfile) {
    $imgfile = (object)$imgfile;
    if ($imgfile->list && empty($imgfile->remove)) {
      $title    = $imgfile->description ? $imgfile->description : $imgfile->filename;
      $text    = '<img src="' . base_path() . $imgfile->mainpath . '" />';
      $options = array(
        'attributes' => array(
          'title' => t('@title', array('@title' => $title)),
        ),
        'html' => TRUE,        
      );
      $output .= l($text, $imgfile->origpath, $options);
    }
  }
  $output .= '</p>';
  $output .= '</div>';
  return $output;
}

/**
 * Displays file attachments in lightbox With Grouping style
 *
 * @ingroup themeable
 */
function theme_jimage_lightbox($jimage) {
  // Display main image
  $imgfiles = $jimage['image'];
  $imgpath  = $jimage['path'];
  drupal_add_css(drupal_get_path('module', 'jimage') .'/jimage.css');
  $output  = '';
  $output .= '<div class="container">';
  $output .= '<br class="clearboth">'; 
  $output .= '<p class="imageOptions">';
  // Display thumbnails
  foreach ($imgfiles as $imgfile) {
    $imgfile = (object)$imgfile;
    if ($imgfile->list && empty($imgfile->remove)) {
      $title    = $imgfile->description ? $imgfile->description : $imgfile->filename;
      $text     = '<img src="' . base_path() . $imgfile->thumbpath . '" />';
      $rel      = 'lightbox[$imgfile->filename]';
      $options = array(
        'attributes' => array(
          'title' => t('@title', array('@title' => $title)),
          'rel'   => t('@rel', array('@rel' => $rel)),          
        ),
        'html' => TRUE,        
      );
      $output .= l($text, $imgfile->mainpath, $options);
    }
  }
  $output .= '</p>';
  $output .= '</div>';
  return $output;
}

/**
 * Displays file attachments in lightbox Slideshow style
 *
 * @ingroup themeable
 */
function theme_jimage_lightshow($jimage) {
  // Display main image
  $imgfiles = $jimage['image'];
  $imgpath  = $jimage['path'];
  drupal_add_css(drupal_get_path('module', 'jimage') .'/jimage.css');
  $output  = '';
  $output .= '<div class="container">';
  $output .= '<br class="clearboth">'; 
  $output .= '<p class="imageOptions">';
  // Display thumbnails
  foreach ($imgfiles as $imgfile) {
    $imgfile = (object)$imgfile;
    if ($imgfile->list && empty($imgfile->remove)) {
      $title    = $imgfile->description ? $imgfile->description : $imgfile->filename;
      $text     = '<img src="' . base_path() . $imgfile->thumbpath . '" />';
      $rel      = 'lightshow[$imgfile->filename]';
      $options = array(
        'attributes' => array(
          'title' => t('@title', array('@title' => $title)),
          'rel'   => t('@rel', array('@rel' => $rel)),          
        ),
        'html' => TRUE,        
      );
      $output .= l($text, $imgfile->mainpath, $options);
    }
  }
  $output .= '</p>';
  $output .= '</div>';
  return $output;
}