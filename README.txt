
Readme
-------

A module to show images attached to a node in a slideimage display style using jQuery.

-- REQUIREMENTS --

* Upload

Installation
-------------

* Copy jimage module to your modules directory and enable it on the modules page.

* Go to Home � Administer � Site configuration - Jimage Settings
  Then change settings as desired.

* Go to Home � Administer � Content management - Content types
  Then Enable or Disable Jimage in Workflow settings options

TODO
-----

- TBD

Current Maintainers
-------------------
coupet

Original Author
---------------
Darly Coupet <darlycoupet@gmail.com>